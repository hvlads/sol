#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
import logging
import argparse
from pprint import pprint

from wallets.sol import SolanaWallet
from wallets.wallet import db, Config, Address


async def listen_sockets(funcs_):
    await asyncio.gather(*funcs_)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sync', help='synchronization', nargs='?', default=False)

    parser.add_argument('-c', '--create', help='create wallet', action='store_true')
    parser.add_argument('-a', '--add', help='add new address', action='store_true')
    parser.add_argument('-b', '--balance', help='show balance', action='store_true')
    parser.add_argument('-i', '--info', help='show wallet info', action='store_true')
    parser.add_argument('--tx', help='transaction')
    parser.add_argument('--send', help='send coins', action='store_true')

    parser.add_argument('-n', '--network', help='network id (mainnet, shasta)', default='mainnet')
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)
    wallet = SolanaWallet(network=args.network, path='solana.db')
    loop = asyncio.get_event_loop()


    if args.create:
        db.init('solana.db')
        if Config.table_exists():
            print('Wallet already created')
        else:
            print('Wallet creation')
            Config.create_table()
            Config.create(key='SOL-height', value=0)
            Config.create(key='SOL-time', value=0)
            Address.create_table()
            address = wallet.newAddress()
            print(address)


    if args.add:
        address = wallet.newAddress()
        print(address)


    if args.balance:
        for i, address in enumerate(wallet.getAddresses()):
            print(i + 1, address, loop.run_until_complete(wallet.getBalance(address)))
        loop.run_until_complete(wallet.rpca.client.close())


    if args.info:
        print('Height', wallet.getHeight())
        print('Main', wallet.main)
        print(loop.run_until_complete(wallet.getSync()))


    if args.tx:
        print(args.tx)
        pprint(loop.run_until_complete(wallet.getTx(args.tx)))


    if args.send:
        print('send')
        address = input('Send to: ')
        amount = input('Amount: ')
        includefee_input = input("Includefee. Enter yes or no: ")
        includefee = False
        if includefee_input == "yes":
            includefee = False
        print('Send %s SOL to %s (%s)' % (amount, address, args.network))
        txid = loop.run_until_complete(wallet.send(address, amount, includefee))
        print('txid:', txid)


    if args.sync is not False:
        loop.run_until_complete(wallet.updateAddresses())
        funcs = []
        if args.sync is None:
            print('Sync from', wallet.getHeight())
        elif args.sync.isnumeric():
            wallet.setHeight(int(args.sync))
        else:
            info = loop.run_until_complete(wallet.rpca.client.get_epoch_info())
            number = info['result']['blockHeight']
            print('Sync from now', number)
            wallet.setHeight(number)
        for address in wallet.addresses:
            funcs.append(wallet.handler(address))
        loop.run_until_complete(listen_sockets(funcs))
