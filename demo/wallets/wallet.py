#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import os
import time
from peewee import *
from decimal import Decimal


db = SqliteDatabase(None)


class Config(Model):
    key   = CharField(primary_key=True, max_length=8)
    value = CharField(max_length=8)
    class Meta:
        database = db
        table_name = 'config'


class Address(Model):
    ccy     = CharField(max_length=5)
    address = CharField(max_length=42)
    amount  = DecimalField(max_digits=16, decimal_places=8)
    seed    = CharField(max_length=64, null=True)
    reg     = TimestampField()
    class Meta:
        database = db
        table_name = 'addresses'



class WalletError(Exception):
    def __init__(self, code=0, message=''):
        self.errno   = code
        self.message = message
    
    def __str__(self):
        if self.message:
            return '%s (%s)' % (self.message, self.errno)
        else:
            return 'WalletError (%s)' % (self.errno)


class WalletTxNotFound(WalletError):
    def __init__(self, ):
        WalletError.__init__(self, 1001, 'TX not found')



class BaseWallet():
    CCY   = None
    CHAIN = None

    def __init__(self, path=None):
        self.db = db
        if path is not None:
            self.db.init(path)
        self.main = self.getMain()

    def getMain(self):
        try:
            main = Address.get(Address.seed.is_null(False))
            return main.address
        except Exception as e:
            return None

    def setConfig(self, key, value):
        Config.update({Config.value: value}).where(Config.key == key).execute()

    def getConfig(self, key):
        item = Config.get(Config.key == key)
        return item.value

    def getHeight(self):
        item = Config.get(Config.key == self.CHAIN+'-height')
        return int(item.value)

    def setHeight(self, height):
        Config.update({Config.value: height}).where(Config.key == self.CHAIN+'-height').execute()

    def getAddresses(self):
        return [i.address for i in Address.select()]

    def addAddress(self, address, amount, seed=None):
        Address.create(ccy=self.CCY, address=address, amount=amount, seed=seed, reg=time.time())


    def getActiveAddresses(self, diff=2*3600):
        last = time.time() - diff
        condition = ((Address.reg > last) | (Address.amount > 0)) & (Address.ccy == self.CCY)
        addresses = [i.address for i in Address.select().where(condition)]
        if self.main is not None and self.main not in addresses:
            addresses.insert(0, self.main)
        return addresses


    def getAddressesBalance(self):
        result = []
        for i in Address.select().where((Address.ccy == self.CCY) & (Address.amount > 0)):
            result.append({'address': i.address, 'amount':  i.amount, 'ccy': i.ccy})
        return result


    def getBalance(self, address):
        item = Address.get((Address.ccy == self.CCY) & (Address.address == address))
        return item.amount


    def setBalance(self, address, amount):
        result = (Address
                .update({Address.amount: amount})
                .where((Address.ccy == self.CCY) & (Address.address == address))
                .execute())
        if not result:
            self.addAddress(address, amount)


    def getTotalBalance(self):
        total = Decimal(0)
        for i in Address.select().where(Address.ccy == self.CCY):
            total += i.amount
        return total
