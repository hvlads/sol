#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import os
import json
import time
import base58
import random
import aiohttp
import asyncio
import logging
import requests

import decimal
from decimal import Decimal
from eth_keys import KeyAPI
from eth_account import Account
from collections import defaultdict

from .wallet import WalletError, WalletTxNotFound
from .wallet import Config, Address, BaseWallet, db


class TronAddress():
    def __init__(self, address=None, pubkey=None, seed=None):
        if seed is None:
            self.address = address
        else:
            self.address = address

    def __str__(self):
        return self.address

    def toHex(self):
        return base58.b58decode_check(self.address).hex()

    @classmethod
    def toHex2(cls, address):
        return base58.b58decode_check(address).hex()

    @classmethod
    def fromHex(cls, h):
        address = base58.b58encode_check(bytes.fromhex(h))
        return cls(address.decode('utf-8'))

    @classmethod
    def valid(cls, address):
        try:
            raw = cls.toHex2(address)
            return address.startswith('T') and len(raw) == 42
        except Exception as e:
            return False



class TronAsyncRPC():
    def __init__(self, network):
        self.network = network
        if self.network == 'mainnet':
            self.node = 'https://api.trongrid.io'
        if self.network == 'shasta':
            self.node = 'https://api.shasta.trongrid.io'


    def toAmount(self, amount):
        return Decimal(amount) / Decimal('1E6')


    async def request(self, method, params={}):
        url = self.node + method
        try:
            async with aiohttp.ClientSession() as session:
                if params:
                    async with session.post(url, json=params) as response:
                        response.raise_for_status()
                        return json.loads(await response.text())
                else:
                    async with session.get(url) as response:
                        response.raise_for_status()
                        return json.loads(await response.text())
        except Exception as e:
            logging.getLogger('wallet').error('RPC %s %s', url, e)


    async def getBalance(self, address):
        resp = await self.request('/wallet/getaccount', {'address': address, 'visible': True})
        return self.toAmount(resp.get('balance', 0))



class TronWallet(BaseWallet):
    CCY   = 'TRX'
    CHAIN = 'TRX'


    def __init__(self, network, *args, **kwargs):
        self.network = network
        self.rpca = TronAsyncRPC(network)
        super().__init__(*args, **kwargs)


    def getMain(self):
        try:
            item = Address.get()
            return item.address
        except Exception as e:
            return


    def newAddress(self):
        seed = os.urandom(32).hex()
        key = KeyAPI.PrivateKey(bytes.fromhex(seed))
        hexAddress = '41' + key.public_key.to_address()[2:]
        address = base58.b58encode_check(bytes.fromhex(hexAddress))
        address = address.decode('utf-8')
        Address.create(ccy=self.CCY, address=address, amount='0', seed=seed, reg=time.time())
        return address

    def isAddress(self, address):
        return TronAddress.valid(address)

    def toAmount(self, amount):
        return Decimal(amount) / Decimal('1E6')

    def fromAmount(self, amount):
        return int(Decimal(amount) * Decimal('1E6'))

    def getFee(self, force=False):
        return Decimal('0.003')

    def getLimit(self):
        total = self.getTotalBalance()
        sendMax = self.getBalance(self.main)
        return {
            'recvMin':   Decimal('0'),
            'recvMax':   None,
            'recvTotal': None,
            'sendMin':   Decimal('0'),
            'sendMax':   sendMax,
            'sendTotal': total,
            'total':     total
        }



    async def getTx(self, txid):
        tx = await self.rpca.request('/wallet/gettransactionbyid', {'value': txid})
        if not tx:
            logging.getLogger('wallet').error('getTx empty1 %s %s', txid, tx)
            raise WalletTxNotFound()

        info = await self.rpca.request('/wallet/gettransactioninfobyid', {'value': txid})
        if not info:
            logging.getLogger('wallet').error('getTx empty2 %s %s', txid, info)
            raise WalletTxNotFound()

        addresses = self.getAddresses()

        io = []
        fee = self.toAmount(info.get('fee', 0))

        for contract in tx['raw_data']['contract']:
            if contract['type'] == 'TransferContract':
                amount = self.toAmount(contract['parameter']['value']['amount'])
                fr = TronAddress.fromHex(contract['parameter']['value']['owner_address'])
                to = TronAddress.fromHex(contract['parameter']['value']['to_address'])
                io.append({
                    'dir':     'send',
                    'address': fr.address,
                    'tag':     None,
                    'ccy':     'TRX',
                    'amount':  amount,
                    'account': True if fr.address in addresses else None
                })
                io.append({
                    'dir':     'recv',
                    'address': to.address,
                    'tag':     None,
                    'ccy':     'TRX',
                    'amount':  amount,
                    'account': True if to.address in addresses else None
                })

            if contract['type'] == 'TransferAssetContract':
                if contract['parameter']['value']['asset_name'] == '31303032303030':
                    ccy = 'BTT'
                    amount = self.toAmount(contract['parameter']['value']['amount'])
                    fr = TronAddress.fromHex(contract['parameter']['value']['owner_address'])
                    to = TronAddress.fromHex(contract['parameter']['value']['to_address'])
                    io.append({
                        'dir':     'send',
                        'address': fr.address,
                        'tag':     None,
                        'amount':  amount,
                        'ccy':     ccy,
                        'account': True if fr.address in addresses else None
                    })
                    io.append({
                        'dir':     'recv',
                        'address': to.address,
                        'tag':     None,
                        'amount':  amount,
                        'ccy':     ccy,
                        'account': True if to.address in addresses else None
                    })

            if contract['type'] == 'TriggerSmartContract' and info and info['receipt']['result'] == 'SUCCESS':
                for log in info.get('log', []):
                    contract = TronAddress.fromHex('41' + log['address'])
                    if log['topics'][0] == 'ddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef' and contract.address == 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t':
                        amount = self.toAmount(int(log['data'], 16))
                        fr = TronAddress.fromHex('41' + log['topics'][1][-40:])
                        to = TronAddress.fromHex('41' + log['topics'][2][-40:])
                        io.append({
                            'dir':     'send',
                            'address': fr.address,
                            'tag':     None,
                            'ccy':     'USDT',
                            'amount':  amount,
                            'account': True if fr.address in addresses else None
                        })
                        io.append({
                            'dir':     'recv',
                            'address': to.address,
                            'tag':     None,
                            'ccy':     'USDT',
                            'amount':  amount,
                            'account': True if to.address in addresses else None
                        })


                for itx in info.get('internal_transactions', []):
                    if 'callValue' in itx['callValueInfo'][0]:
                        amount = self.toAmount(itx['callValueInfo'][0]['callValue'])
                        fr = TronAddress.fromHex(itx['caller_address'])
                        to = TronAddress.fromHex(itx['transferTo_address'])

                        io.append({
                            'dir':     'send',
                            'address': fr.address,
                            'tag':     None,
                            'ccy':     'TRX',
                            'amount':  amount,
                            'account': True if fr.address in addresses else None
                        })
                        io.append({
                            'dir':     'recv',
                            'address': to.address,
                            'tag':     None,
                            'ccy':     'TRX',
                            'amount':  amount,
                            'account': True if to.address in addresses else None
                        })
        
        height = self.getHeight()
        if 'blockNumber' in info and 'blockTimeStamp' in info:
            confirmations = height - info['blockNumber']
            timestamp = info['blockTimeStamp'] // 1000
        else:
            confirmations = 0
            timestamp = 0

        return {
            'txid':          txid,
            'io':            io,
            'height':        height,
            'fee':           fee,
            'time':          timestamp,
            'confirmations': confirmations
        }


    async def send(self, to, amount, includefee=False):
        main = Address.get(Address.seed.is_null(False))
        return await self.transfer(main.address, to, amount, includefee=includefee)


    async def transfer(self, fr, to, amount, includefee=False):
        logging.getLogger('wallet.trx').info('Sending %s %s to %s', amount, self.CCY, to)
        fr = Address.get(Address.address == fr)
        data = {
            'owner_address': fr.address,
            'to_address':    to,
            'amount':        self.fromAmount(amount),
            'visible':       True
        }

        for i in range(3):
            tx = await self.rpca.request('/wallet/createtransaction', data)
            sig = Account.signHash(tx['txID'], fr.seed)
            tx['signature'] = [sig['signature'].hex()[2:]]

            if tx['raw_data']['expiration'] < tx['raw_data']['timestamp']:
                logging.getLogger('wallet.trx').error('expiration %s %s %s', to, amount, self.CCY)
            else:
                break

        sent = False
        for i in range(3):
            resp = await self.rpca.request('/wallet/broadcasttransaction', tx)
            logging.getLogger('wallet.trx').info('broadcast %s', resp)
            sent = sent or resp.get('result')
            if resp.get('code') == 'DUP_TRANSACTION_ERROR':
                break
        if sent:
            return resp['txid']
        logging.getLogger('wallet.trx').error('broadcast %s %s %s', to, amount, self.CCY)
        raise WalletTxNotFound()


    async def getSync(self):
        block = await self.rpca.request('/wallet/getnowblock')
        timestamp = float(self.getConfig('TRX-time'))
        return {
            'online': True,
            'synced': (time.time()-timestamp) < 5*60,
            'time':   timestamp,
            'height': self.getHeight(),
            'top':    block['block_header']['raw_data']['number']
        }


    # Update addresses
    async def updateAddresses(self):
        addresses = {}
        for item in self.getActiveAddresses(diff=2*24*3600):
            h = base58.b58decode_check(item)
            addresses[h.hex()] = 0
        self.addressesDict = addresses

    # Process tx
    async def processTX(self, txid):
        logging.getLogger('wallet.trx').info('[+] TX %s', txid)


    # Process block
    async def processBlock(self, block):
        if 'block_header' not in block:
            logging.getLogger('wallet.trx').error('[-] Block empty %s', block)
            return

        txids = set()
        number = block['block_header']['raw_data']['number']
        transactions = block.get('transactions', [])
        if number % 5 == 0:
            logging.getLogger('wallet.trx').info('[+] Block TRX %s %s', number, len(transactions))

        for tx in transactions:
            for contract in tx['raw_data']['contract']:
                if contract['type'] == 'TransferContract':
                    fr = contract['parameter']['value']['owner_address']
                    to = contract['parameter']['value']['to_address']
                    # print('Transfer', fr, to , amount)
                    if fr in self.addressesDict or to in self.addressesDict:
                        txids.add(tx['txID'])
                if contract['type'] == 'TransferAssetContract':
                    fr = contract['parameter']['value']['owner_address']
                    to = contract['parameter']['value']['to_address']
                    # print('Transfer', fr, to , amount)
                    if fr in self.addressesDict or to in self.addressesDict:
                        txids.add(tx['txID'])
                if contract['type'] == 'TriggerSmartContract' and contract['parameter']['value'].get('data', '').startswith('a9059cbb'):
                    ca = contract['parameter']['value']['contract_address']
                    fr = contract['parameter']['value']['owner_address']
                    to = '41' + contract['parameter']['value']['data'][32:72]
                    # USDT TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t
                    if ca == '41a614f803b6fd780986a42c78ec9c7f77e6ded13c':
                        if fr in self.addressesDict or to in self.addressesDict:
                            txids.add(tx['txID'])

        logs = await self.rpca.request('/wallet/gettransactioninfobyblocknum', {'num': number})
        for log in logs:
            for itx in log.get('internal_transactions', []):
                try:
                    amount = self.toAmount(itx['callValueInfo'][0]['callValue'])
                    fr = itx['caller_address']
                    to = itx['transferTo_address']
                    if fr in self.addressesDict or to in self.addressesDict:
                        txids.add(log['id'])
                except KeyError as e:
                    if e.args[0] != 'callValue':
                        logging.getLogger('wallet').error('Internal %s', e)
                except Exception as e:
                    logging.getLogger('wallet').error('Internal %s', e)
        
        for txid in txids:
            await self.processTX(txid)



    async def handler(self):
        while True:
            try:
                block = cblock = await self.rpca.request('/wallet/getnowblock')
                current = cblock['block_header']['raw_data']['number']
                timestamp = cblock['block_header']['raw_data']['timestamp']/1000
                last = self.getHeight()

                for number in range(last + 1, current + 1):
                    if number == current:
                        block = cblock
                    else:
                        block = await self.rpca.request('/wallet/getblockbynum', {'num': number})

                    if 'block_header' not in block:
                        logging.getLogger('wallet').error('Block empty %s %s', number, block)
                        break

                    asyncio.ensure_future(self.processBlock(block))
                    timestamp = block['block_header']['raw_data']['timestamp'] / 1000
                    self.setHeight(number)
                    self.setConfig('TRX-time', timestamp)
                    
                    if number != current:
                        await asyncio.sleep(0.1)

                diff = time.time() - timestamp

                if diff < 3:
                    await asyncio.sleep(3)
                else:
                    await asyncio.sleep(1)
            except Exception as e:
                logging.getLogger('wallet').error('Main handler %s', e)
                logging.getLogger('wallet').exception('Main handler')
                await asyncio.sleep(1)
