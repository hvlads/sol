import asyncio
import logging
import random
import time
from decimal import Decimal
from pprint import pprint
from typing import cast

from solana.rpc.websocket_api import connect
from solana.rpc.websocket_api import SolanaWsClientProtocol
from solana.keypair import Keypair
from solana.publickey import PublicKey
from solana.rpc.async_api import AsyncClient
from solana.system_program import transfer, TransferParams
from solana.transaction import Transaction
from solana.utils.ed25519_base import is_on_curve

from wallets.wallet import BaseWallet, Address, WalletTxNotFound, WalletError

rpc_endpoints = {
    "mainnet": "https://api.mainnet-beta.solana.com",
    "testnet": "https://api.testnet.solana.com",
    "devnet": "https://api.devnet.solana.com",
    "local": "http://localhost:8899",
}


class SolanaAsyncRPC:
    def __init__(self, network):
        self.network = network
        self.node = rpc_endpoints[network]
        self.client = AsyncClient(self.node)


class SolanaWallet(BaseWallet):
    CCY = "SOL"
    CHAIN = "SOL"

    def __init__(self, network, *args, **kwargs):
        self.network = network
        self.rpca = SolanaAsyncRPC(network)
        self.addresses = []
        super().__init__(*args, **kwargs)

    def toAmount(self, amount):
        return Decimal(amount) / Decimal("1E9")

    def fromAmount(self, amount):
        return int(Decimal(amount) * Decimal("1E9"))

    async def getTx(self, txid):
        tx = await self.rpca.client.get_transaction(txid)
        if "error" in tx:
            raise WalletError(tx["error"]["message"])
        if not tx["result"]:
            logging.getLogger("wallet").error("getTx empty1 %s %s", txid, tx)
            raise WalletTxNotFound()
        io = []
        fee = "{0:.10f}".format(self.toAmount(tx["result"]["meta"]["fee"]))
        amount = abs(
            tx["result"]["meta"]["postBalances"][1]
            - tx["result"]["meta"]["preBalances"][1]
        )
        amount = self.toAmount(amount)
        fr = tx["result"]["transaction"]["message"]["accountKeys"][0]
        to = tx["result"]["transaction"]["message"]["accountKeys"][1]

        addresses = self.getAddresses()

        io.append(
            {
                "dir": "send",
                "address": fr,
                "tag": None,
                "ccy": self.CCY,
                "amount": "{0:.10f}".format(amount),
                "account": True if to in addresses else None,
            }
        )
        io.append(
            {
                "dir": "recv",
                "address": to,
                "tag": None,
                "ccy": self.CCY,
                "amount": "{0:.10f}".format(amount),
                "account": True if to in addresses else None,
            }
        )
        info = await self.rpca.client.get_epoch_info()
        height = info["result"]["blockHeight"]
        block = await self.rpca.client.get_block(tx["result"]["slot"])
        confirmations = height - block["result"]["blockHeight"]
        timestamp = block["result"]["blockTime"]
        await self.rpca.client.close()
        return {
            "txid": txid,
            "io": io,
            "height": height,
            "fee": fee,
            "time": timestamp,
            "confirmations": confirmations,
        }

    def newAddress(self):
        seed = bytes([random.randint(0, 255) for i in range(32)])
        keypair = Keypair.from_seed(seed)
        address = keypair.public_key
        Address.create(
            ccy=self.CCY, address=address, amount="0", seed=seed.hex(), reg=time.time()
        )
        return address

    def isAddress(self, address: str):
        return is_on_curve(PublicKey(address))

    async def getBalance(self, address):
        res = await self.rpca.client.get_balance(PublicKey(address))
        balance = res["result"]["value"]
        return "{0:.10f}".format(self.toAmount(balance))

    async def getFee(self):
        res = await self.rpca.client.get_fees()
        return self.toAmount(
            res["result"]["value"]["feeCalculator"]["lamportsPerSignature"]
        )

    async def transfer(self, fr, to, amount, includefee=False):
        from_amount = self.fromAmount(amount)
        fee = await self.getFee()
        if includefee:
            from_amount = self.fromAmount(amount) - fee
        logging.getLogger("solana.trx").info(
            "Sending %s %s to %s", amount, self.CCY, to
        )
        fr = Address.get(Address.address == fr)
        sender = Keypair.from_secret_key(bytes.fromhex(fr.seed))
        txn = Transaction().add(
            transfer(
                TransferParams(
                    from_pubkey=sender.public_key,
                    to_pubkey=to,
                    lamports=int(from_amount),
                )
            )
        )
        tr = await self.rpca.client.send_transaction(txn, sender)
        await self.rpca.client.close()
        return tr["result"]

    async def send(self, to, amount, includefee=False):
        main = Address.get(Address.seed.is_null(False))
        return await self.transfer(main.address, to, amount, includefee=includefee)

    # Update addresses
    async def updateAddresses(self):
        for item in self.getActiveAddresses(diff=2 * 24 * 3600):
            self.addresses.append(item)

    async def processBlock(self, block):
        _id = block["id"]
        transactions = block["result"]["transactions"]
        txids = set()
        logging.getLogger("wallet.trx").info(
            "[+] Block SOL %s %s", _id, len(transactions)
        )
        for tx in transactions:
            if set(self.addresses) & set(tx["transaction"]["message"]["accountKeys"]):
                txids.add(tx["transaction"]["signatures"][0])
        for txid in txids:
            await self.processTX(txid)

    async def getSync(self):
        info = await self.rpca.client.get_epoch_info()
        top = info["result"]["blockHeight"]
        timestamp = float(self.getConfig("SOL-time"))
        await self.rpca.client.close()
        return {
            "online": True,
            "synced": (time.time() - timestamp) < 5 * 60,
            "time": timestamp,
            "height": self.getHeight(),
            "top": top,
        }

    async def handler(self, address):
        conn = connect(
            uri=rpc_endpoints[self.network]
            .replace("https://", "ws://")
            .replace("http://", "ws://")
        )
        print(address)
        async with cast(SolanaWsClientProtocol, conn) as websocket:
                await websocket.account_subscribe(
                    pubkey=address, encoding="jsonParsed"
                )
                while True:
                    msg = await websocket.recv()
                    if hasattr(msg.result, "context"):

                        signature = await self.rpca.client.get_signatures_for_address(
                            address, limit=1
                        )
                        if not signature["result"]:
                            continue
                        transaction = await self.rpca.client.get_transaction(
                            signature["result"][0]["signature"]
                        )
                        block = await self.rpca.client.get_block(
                            transaction["result"]["slot"]
                        )
                        asyncio.ensure_future(self.processBlock(block))

                        self.setHeight(block["result"]["blockHeight"])
                        self.setConfig("SOL-time", block["result"]["blockTime"])

    # Process tx
    async def processTX(self, txid):
        logging.getLogger("wallet.trx").info("[+] TX %s", txid)
