#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import asyncio
import logging
import argparse
from wallets.tron import TronWallet, Config, Address, db


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sync', help='synchronization', nargs='?', default=False)

    parser.add_argument('-c', '--create', help='create wallet', action='store_true')
    parser.add_argument('-a', '--add', help='add new address', action='store_true')
    parser.add_argument('-b', '--balance', help='show balance', action='store_true')
    parser.add_argument('-i', '--info', help='show wallet info', action='store_true')
    parser.add_argument('--tx', help='transaction')
    parser.add_argument('--send', help='send coins', action='store_true')

    parser.add_argument('-n', '--network', help='network id (mainnet, shasta)', default='mainnet')
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)
    wallet = TronWallet(network=args.network, path='trx.db')
    loop = asyncio.get_event_loop()


    if args.create:
        db.init('trx.db')
        if Config.table_exists():
            print('Wallet already created')
        else:
            print('Wallet creation')
            Config.create_table()
            Config.create(key='TRX-height', value=0)
            Config.create(key='TRX-time', value=0)
            Address.create_table()
            address = wallet.newAddress()
            print(address)


    if args.add:
        address = wallet.newAddress()
        print(address)


    if args.balance:
        for i, address in enumerate(wallet.getAddresses()):
            print(i + 1, address, wallet.getBalance(address))


    if args.info:
        print('Height', wallet.getHeight())
        print('Main', wallet.main)
        print(loop.run_until_complete(wallet.getSync()))


    if args.tx:
        print(args.tx)
        print(loop.run_until_complete(wallet.getTx(args.tx)))


    if args.send:
        print('send')
        address = input('Send to: ')
        amount = input('Amount: ')
        print('Send %s TRX to %s (%s)' % (amount, address, args.network))
        txid = loop.run_until_complete(wallet.send(address, amount))
        print('txid:', txid)


    if args.sync is not False:
        loop.run_until_complete(wallet.updateAddresses())
        if args.sync is None:
            print('Sync from', wallet.getHeight())
            loop.run_until_complete(wallet.handler())
        elif args.sync.isnumeric():
            wallet.setHeight(int(args.sync))
            loop.run_until_complete(wallet.handler())
        else:
            block = loop.run_until_complete(wallet.rpca.request('/wallet/getnowblock'))
            number = block['block_header']['raw_data']['number']
            print('Sync from now', number)
            wallet.setHeight(number)
            loop.run_until_complete(wallet.handler())
