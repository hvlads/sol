#!/bin/bash
# Validator Requirements
# https://docs.solana.com/running-validator/validator-reqs
yum update
yum -y install wget bzip2

if [ ! -f solana-release-x86_64-unknown-linux-gnu.tar.bz2 ]; then
     # https://github.com/solana-labs/solana/releases/tag/v1.9.1
     wget https://github.com/solana-labs/solana/releases/download/v1.9.1/solana-release-x86_64-unknown-linux-gnu.tar.bz2
     tar jxf solana-release-x86_64-unknown-linux-gnu.tar.bz2
fi
cd solana-release/ || exit
export PATH=$PWD/bin:$PATH
solana --version
solana transaction-count
solana config set --url https://api.devnet.solana.com
$(command -v solana-sys-tuner) --user $(whoami) > sys-tuner.log 2>&1 &
if [ ! -f ~/validator-keypair.json ]; then
    # Create an identity keypair for your validator by running:
    solana-keygen new -o ~/validator-keypair.json

    solana-keygen pubkey ~/validator-keypair.json
    # Set the solana configuration to use your validator keypair for all following commands:
    solana config set --keypair ~/validator-keypair.json
fi
# Airdrop yourself some SOL to get started:
solana airdrop 1

# To view your current balance:
solana balance --lamports

# To create your authorized-withdrawer keypair:
if [ ! -f ~/authorized-withdrawer-keypair.json ]; then
   solana-keygen new -o ~/authorized-withdrawer-keypair.json
fi
# If you haven’t already done so, create a vote-account keypair and create the vote account on the network. 
# If you have completed this step, you should see the “vote-account-keypair.json” in your Solana runtime directory:
if [ ! -f ~/vote-account-keypair.json ]; then
   solana-keygen new -o ~/vote-account-keypair.json
fi
# The following command can be used to create your vote account on the blockchain with all the default options:
solana create-vote-account ~/vote-account-keypair.json ~/validator-keypair.json ~/authorized-withdrawer-keypair.json

# Connect to the cluster by running:
solana-validator \
  --identity ~/validator-keypair.json \
  --vote-account ~/vote-account-keypair.json \
  --rpc-port 8899 \
  --entrypoint entrypoint.devnet.solana.com:8001 \
  --limit-ledger-size \
  --log ~/solana-validator.log

# Confirm your validator is connected to the network by opening a new terminal and running:
# solana gossip



